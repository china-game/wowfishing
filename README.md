# wowfishing

#### 介绍
魔兽世界的自动钓鱼程序,支持最新版和经典怀旧服;

#### 软件界面

![截图](screen.png)

#### 下载地址

* [点击转到下载页面](https://gitee.com/milaoshu1020/wowfishing/releases)

如果运行不了请下载 .Net Framework 4.0 并安装:

* [.Net Framework 4.0 下载地址](https://www.microsoft.com/zh-cn/download/details.aspx?id=17718);

#### VBX脚本的使用

* 运行脚本需要系统已经安装了 [NScript](https://github.com/milaoshu1020/NScript)([下载地址](https://github.com/milaoshu1020/NScript/releases));
* 安装完毕以后,双击VBX脚本即可运行;
* 打开任务管理器,结束"wnscript.exe"进程可以终止脚本运行;
* 你可以用记事本或者其他编辑器查看甚至编辑VBX文件;

#### 常见问题

* [点击查看](https://gitee.com/milaoshu1020/wowfishing/wikis/)

#### 捐赠

* [捐赠地址](DONATE.md)
